interface StoreData {
  id: number
  name: string
  store_image: string
  title_name: string
  meta_title: string
  meta_description: string
  meta_keywords: string
  web_url: string
  affiliate_url: string
  content: string
  description: string
  page_title: string
}

export interface StoreRating {
  headers: object | {}
  original: {
    message: string
    average: number
    total: number
  }
  exception: null | any
}

export interface RelatedCategories {
  id: number
  name: string
  image: string
}

export interface TableCoupons {
  title: string
}

export interface Faqs {
  _q: string
  _sort: null | any
  _ans: string
}

export interface RelatedStores {
  slug: string
  name: string
}

export interface Breadcrumd {
  slug: string
  name: string
}

export default StoreData
