export interface StoreSearch {
  search_keyword: string;
}

export interface Subscribe {
  email: string;
}
