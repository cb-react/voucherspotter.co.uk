interface coupons {
  id: number;
  title: string;
  code?: string;
  verified: 0 | 1;
  exclusive: 0 | 1;
  coupon_terms: string;
  store_id: number;
  store_name: string;
  store_image: string;
  store_slug: string;
  code_status: "Code" | "Deal";
}

interface categories {
  id: number;
  name: string;
  page_url: string;
}

interface stores {
  id: number;
  store_name: string;
  store_image: string;
  store_slug: string;
}

export default interface SearchResult {
  coupons?: [coupons];
  categories?: [categories];
  stores: [stores];
}
