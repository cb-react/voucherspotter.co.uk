export default interface Store {
  slug: string;
  sname: string;
  description: string;
  ratingPercent: number;
  totalRatingPercent: number;
  bestRating: number;
  worstRating: number;
  image: string;
  title: string;
  category: {
    categoryName: string;
    categorySlug: string;
  };
  storeUrl: string;
  exclusive: 0 | 1;
}
