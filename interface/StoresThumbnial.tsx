export default interface StoresThumbnial {
  id: number
  store_name: string
  slug: string
  image: string
}
