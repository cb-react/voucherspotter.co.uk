export default interface CategoryDetails {
  id: number;
  meta_title: string;
  meta_description: string;
  meta_keyword: string;
  page_type: "category";
  name: string;
  image: string;
  page_url: string;
}
