export default interface CouponPopup {
  id: number
  image: string
  title: string
  code: string
  store_name: string
  terms: string
  date_expiry: string
  hide_expiry: 0 | 1
  page_type?: string
  slug: string
  store_id: number
  type: "Code" | "Deal"
  verified: 0 | 1
  viewed: number
}
