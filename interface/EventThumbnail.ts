export default interface EventThumbnail {
  id: number;
  name: string;
  slug: string;
  cover_image: string;
  image: string;
}
