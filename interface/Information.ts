interface Information {
  id: number;
  status: 0 | 1;
  sort_order: number;
  description: string;
  title: string;
  meta_title: string;
  bottom: number;
  top: number;
  created_at: string;
  updated_at: string;
  json_data: string;
}

interface Data {
  meta_description: string;
  meta_keywords: string;
  information: Information;
}

export default interface Content {
  data?: Data;
  title?: string;
}
