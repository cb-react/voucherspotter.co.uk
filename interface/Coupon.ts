export default interface Coupon {
  id: number
  image: string
  store_name?: string
  sname?: string
  title: string
  terms: string
  type: "Code" | "Deal"
  verified: boolean
  sponsored: boolean
  viewed: number
  date_expiry: string
  desc: string
  slug: string
  hide_expiry: 0 | 1
  exclusive: 0 | 1
  store_id: number
  page_type?: string
  code?: string
  sid?: number
}
