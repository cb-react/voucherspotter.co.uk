import CouponPopup from "./CouponPopup";

export default interface PageProps {
  page_type?: "store" | "category" | "information" | "event";
  page_title?: string;
  Popup?: CouponPopup;
  data?: any;
  notFound?: boolean;
}
