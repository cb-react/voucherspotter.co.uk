export default interface Category {
  id: number;
  name: string;
  page_url: string;
  image: string;
}
