export default interface StoreCoupon {
  id: number
  title: string
  verified: 0 | 1
  code_status: "Deal" | "Code"
  expiry: string
  viewed: number
  coupon_terms: string
  exclusive: 0 | 1
}
