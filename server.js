// server.js
const { createServer } = require("http")
const express = require("express")
const { parse } = require("url")
const next = require("next")
const storesearch = require("./routes/storesearch")
const storerating = require("./routes/storerating")
const subscribe = require("./routes/subscribe")
const coupon = require("./routes/coupon")
const affiliate = require("./routes/affiliate")

const dev = process.env.NODE_ENV !== "production"
const app = next({ dev })
const handle = app.getRequestHandler()

app.prepare().then(() => {
  const server = express()
  server
    .use("/storesearch", storesearch)
    .use("/storerating", storerating)
    .use("/subscribe", subscribe)
    .use("/coupon", coupon)
    .use("/r", affiliate)
    .get("*", (req, res) => {
      return handle(req, res)
    })
    .listen(80, (err) => {
      if (err) throw err
      console.log("> Ready on http://localhost/")
    })
})
