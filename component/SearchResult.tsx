import Link from "next/link"
import React, { useEffect, useRef, useState } from "react"
import fetchData from "../clientFetchData"
import ISearchResult from "../interface/SearchResult"

interface SearchResultProps {
  query: string
  visibility: boolean
}

const SearchResult = ({ query, visibility }: SearchResultProps) => {
  const [results, setResults] = useState<ISearchResult | null>(null)

  const schref = useRef(null)
  let controller: null | AbortController = null

  useEffect(() => {
    controller = new window.AbortController()
  }, [])

  useEffect(() => {
    const abortFetching = () => {
      controller && controller.abort()
    }
    let oldSearchQuery: null | string = null
    async function fetchSearch() {
      if (oldSearchQuery !== query && query.length > 0) {
        abortFetching()
        controller = new window.AbortController()
        await fetchData(
          "storesearch/search",
          "POST",
          {
            search_keyword: query,
          },
          undefined,
          {
            signal: controller.signal,
          }
        ).then((results) => {
          setResults(results)
          oldSearchQuery = query
        })
      } else {
        setResults(results)
      }
    }
    visibility ? fetchSearch() : setResults(null)
  }, [query, visibility])

  return (
    <div
      className="resultsearch"
      style={{ display: visibility ? "block" : "none" }}
      ref={schref}
    >
      {visibility &&
        query.length > 0 &&
        results &&
        results.stores.map((r: any, k: number) => (
          <Link key={k} href={r.store_slug}>
            {r.store_name}
          </Link>
        ))}
    </div>
  )
}

export default SearchResult
