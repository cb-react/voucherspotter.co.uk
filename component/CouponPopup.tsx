import Head from "next/head"
import React, { createRef, useEffect, useState } from "react"
import { CopyToClipboard } from "react-copy-to-clipboard"
import fetchData from "../clientFetchData"
import Coupon from "../interface/Coupon"

interface CouponPopupProps {
  defaultTitle: string
  id: number
}

interface CouponPopup extends Coupon {
  aff_url: string
  affiliate_url: string
}
const CouponPopup = ({ defaultTitle, id }: CouponPopupProps) => {
  const [data, setData] = useState<CouponPopup>()
  let copiedTimeout: number
  let [copied, setcopied] = useState<boolean>(false)
  let copiedDisable = () =>
    (copiedTimeout = window.setTimeout(() => setcopied(false), 3000))
  let copyCode = () => (
    clearTimeout(copiedTimeout), setcopied(true), copiedDisable()
  )
  let [terms, setTerms] = useState(0)
  let TermsInner = createRef<any>()
  let [modalVs, setModalVs] = useState("block")
  let [headTitle, setHeadTitle] = useState<string>(defaultTitle)
  useEffect(() => {
    fetchData("coupon", "POST", { id }).then((res) => {
      setData(res.coupon_data)
    })
  }, [])

  useEffect(() => {
    if (modalVs === "block" && data) setHeadTitle(data.title)
    else setHeadTitle(defaultTitle)
  }, [data, modalVs])
  if (data) {
    const storeRedirect = () => {
      console.log(data)
      window.open(`${process.env.NEXT_PUBLIC_APP_URL}r/${id}`)
    }

    const storeRedirectObj: object = {
      onClick: storeRedirect,
      onKeyUp: (e: KeyboardEvent) => e.key === "Enter" && storeRedirect(),
    }

    let TermsBtn = () => {
      return setTerms(
        TermsInner.current.parentElement.offsetHeight === 0
          ? TermsInner.current.offsetHeight
          : 0
      )
    }
    const closeModal = () => setModalVs("none")

    return (
      <>
        <Head>
          <title>{headTitle}</title>
        </Head>
        <div
          id="gridSystemModalcode"
          className="modal fade in"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="gridModalLabel"
          aria-hidden="false"
          style={{ display: modalVs }}
        >
          <div id="remove">
            <div className="modal-backdrop fade in" onClick={closeModal} />
          </div>
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button
                  type="button"
                  className="close"
                  aria-label="Close"
                  onClick={closeModal}
                >
                  <span aria-hidden="true">×</span>
                </button>
                <h4 className="modal-title" id="gridModalLabel">
                  {data.title}
                </h4>
                <p>
                  {data.code
                    ? `The ${data.sname} website has been opened in a new tab. Simply copy
                and paste the code ${data.code} and enter it at the checkout.`
                    : `This deal has been opened in a new tab, no code is required at checkout.`}
                </p>
              </div>
              <div className="modal-body">
                <div className="col-md-12">
                  <div className="row">
                    {data.code && (
                      <>
                        <div className="code-area">
                          <div className="col-md-8 col-xs-12">
                            <input
                              className={`code${copied ? " copied" : ""}`}
                              id="input_output"
                              defaultValue={data.code}
                              readOnly
                            />
                          </div>
                          <div className="col-md-4  col-xs-12 no-padding">
                            <CopyToClipboard
                              text={data.code}
                              onCopy={() => copyCode()}
                            >
                              <div className="button" id="copy-button">
                                {!copied ? "Copy the coupon code" : "COPIED"}
                              </div>
                            </CopyToClipboard>
                          </div>
                        </div>
                        <div className="clearfix" />
                      </>
                    )}
                    <div
                      className="button-submit"
                      role="button"
                      tabIndex={-1}
                      {...storeRedirectObj}
                    >
                      Visit the store
                    </div>
                    {data.terms && (
                      <>
                        <div
                          role="button"
                          tabIndex={3}
                          className="termsBtn"
                          onClick={TermsBtn}
                          onKeyUp={(e) => e.key === "Enter" && TermsBtn()}
                        >
                          Terms <i className="fa fa-chevron-down" />
                        </div>
                        <div className="termContent" style={{ height: terms }}>
                          <div className="termContentInner" ref={TermsInner}>
                            <h3>Terms &amp; Conditions</h3>
                            <h6 className="bold-black">
                              Expiry:{" "}
                              {data.date_expiry ? data.date_expiry : "Soon"}
                            </h6>
                            <div
                              className="dyncnt"
                              dangerouslySetInnerHTML={{
                                __html: data.terms,
                              }}
                            />
                          </div>
                        </div>
                      </>
                    )}
                  </div>
                </div>
              </div>
              <div className="clearfix" />
            </div>
          </div>
        </div>
      </>
    )
  }
  return <></>
}

export default CouponPopup
