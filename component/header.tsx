import React, {
  ChangeEvent,
  createRef,
  Key,
  KeyboardEvent,
  useState,
} from "react";
import Link from "next/link";
import Image from "next/image";
import SearchResult from "./SearchResult";

const Header = () => {
  const [srVisible, setSrVisible] = useState(false);
  const [search, setSearch] = useState("");
  const storeSearch = (target: HTMLInputElement, key: Key) => {
    const input = target;
    const closeSearch = () => (clearSearch(), input.blur());
    switch (key) {
      case "Enter":
        if (search.length > 0) {
          window.location.href = `${process.env.NEXT_PUBLIC_APP_URL}storesearch?q=${search}`;
        }
        break;
      case "Escape":
        closeSearch();
        break;
      default:
        if (search.length > 0) {
          setSrVisible(true);
        } else {
          setSrVisible(false);
        }
        break;
    }
  };
  const clearSearch = () =>
    search.length > 0 && (setSearch(""), setSrVisible(false));
  return (
    <header role="banner" className="cf">
      <div className="top-wrap">
        <div className="row">
          <div className="large-3 columns">
            <div className="logo">
              <Link href="/">
                <a title={process.env.NEXT_PUBLIC_SITENAME}>
                  <span>
                    <Image
                      src="/img/vs-logo.svg"
                      alt="VoucherSpotter.co.uk"
                      title="VoucherSpotter.co.uk"
                      width={240}
                      height={60}
                    />
                  </span>
                </a>
              </Link>
            </div>
          </div>
          <nav className="large-4 no-padding columns">
            <ul className="rr main-menu" style={{ textAlign: "left" }}>
              <li>
                <Link href="/categories">
                  <a>Categories</a>
                </Link>
              </li>
              <li>
                <Link href="/blog">
                  <a>Blog</a>
                </Link>
              </li>
            </ul>
          </nav>
          <div className="large-5 columns">
            <div className="search-wrap stripe-white">
              <div className="row">
                <div className="small-12 large-12 columns">
                  <label className="search-box">
                    <input
                      type="text"
                      id="search"
                      name="search"
                      placeholder="Eddie Bauer, Baukjen, Zalora"
                      className="input field primary"
                      autoComplete="off"
                      defaultValue={search}
                      onChange={(e) => setSearch(e.target.value)}
                      onKeyUp={(e: KeyboardEvent) =>
                        storeSearch(e.target as HTMLInputElement, e.key)
                      }
                      onFocus={(e) =>
                        e.target.value.length > 0 && setSrVisible(true)
                      }
                    />
                    <a
                      onClick={clearSearch}
                      className="icon clear ir"
                      id="clear-sb"
                    >
                      Clear
                    </a>
                  </label>
                </div>
                <div className="small-4 large-2 columns"></div>
                <SearchResult query={search} visibility={srVisible} />
              </div>
            </div>
          </div>
          <div
            className="searchBg"
            style={{ display: srVisible ? "block" : "none" }}
            onClick={() => setSrVisible(false)}
          />
        </div>
      </div>
    </header>
  );
};

export default Header;
