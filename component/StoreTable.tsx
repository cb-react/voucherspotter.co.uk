import React from "react";
import { TableCoupons } from "../interface/StorePage";

const StoreTable = ({
  title,
  coupons = null,
}: {
  title: String;
  coupons: [TableCoupons] | null;
}) => {
  if (coupons) {
    let date = new Date();
    let expiryDate: string;
    let months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    let dateMonth = `${
      date.getDate() < 10 ? `0${date.getDate()}` : date.getDate()
    } ${months[date.getMonth() - 1]} ${date.getFullYear()}`;
    expiryDate = `${
      date.getDate() < 10 ? `0${date.getDate()}` : date.getDate()
    }/${date.getMonth()}/${date.getFullYear()}`;

    return (
      <div className="strtbl">
        <h2>
          Popular Offers {title} {dateMonth}
        </h2>
        <table>
          <thead>
            <tr>
              <th>
                Offers
                <time>— Last Updated: {expiryDate}</time>
              </th>
              <th>Last Checked</th>
              <th>Code</th>
            </tr>
          </thead>
          <tbody>
            {coupons.map((coupon, k: number) => (
              <tr key={k}>
                <td>{coupon.title}</td>
                <td>{expiryDate}</td>
                <td className="tcntr">*******</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
  return <></>;
};

export default StoreTable;
