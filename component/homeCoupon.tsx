import React, { useState, createRef, DOMElement, Ref, useEffect } from "react"
import Link from "next/link"
import Image from "next/image"
import Coupon from "../interface/Coupon"

interface HomeCouponProps {
  coupon: Coupon
}

type CouponType = "Code" | "Deal"

const HomeCoupon = ({ coupon }: HomeCouponProps) => {
  const dealCodeBtn = (id: number, type: CouponType) => {
    let urlParam = type === "Code" ? "copy" : "shopnow"
    window.open(`${process.env.NEXT_PUBLIC_APP_URL}?${urlParam}=${id}`)
    location.href = `${process.env.NEXT_PUBLIC_APP_URL}r/${id}`
  }

  const Button = ({ id, type }: { id: number; type: CouponType }) => {
    return (
      <a
        className="c_offer input button red secondary"
        onClick={() => dealCodeBtn(id, type)}
      >
        {type === "Code" ? "Show Code" : "Get Deal"}
      </a>
    )
  }

  const [terms, setTerms] = useState(0)

  const TermsInner: { current: any } = createRef()

  const TermsBtn = (e: any) => {
    let termBtnActive: HTMLDivElement | null = document.querySelector(
      ".rr.slides .wrapper-3 .termbtn.active"
    )
    !e.target.classList.contains("active") &&
      termBtnActive &&
      termBtnActive.click()

    return setTerms(
      TermsInner.current.parentElement.offsetHeight === 0
        ? TermsInner.current.offsetHeight + 20
        : 0
    )
  }

  const termsLinksNewTab = () => {
    return coupon.terms.replace("<a", "<a target='_blank'")
  }

  return (
    <li className="large-12 columns">
      <div className="wrapper-3 item-thumb">
        <div className="wrap-new">
          <div className="top large-10 columns">
            <figure>
              <Link href={`/${coupon.slug}`}>
                <a>
                  <Image
                    src={`${process.env.NEXT_PUBLIC_APP_MEDIA}${coupon.image}`}
                    alt={coupon.title}
                    title={coupon.title}
                    width={68}
                    height={68}
                  />
                </a>
              </Link>
            </figure>
            <div className="content-coupon">
              <h2 className="alt">
                <a
                  role="button"
                  onClick={() => dealCodeBtn(coupon.id, coupon.type)}
                  className="c_offer"
                >
                  {coupon.title}
                </a>
              </h2>
              {coupon.terms && (
                <a
                  className={`termbtn${terms > 0 ? " active" : ""}`}
                  onClick={TermsBtn}
                  onKeyUp={(e) => e.key === "Enter" && TermsBtn}
                >
                  Terms <i className="fa fa-chevron-down" />
                </a>
              )}
            </div>
          </div>
          <div className="bottom large-2 columns">
            <Button id={coupon.id} type={coupon.type} />
          </div>
          <div className="clearfix" />
          {coupon.terms && (
            <div>
              <div
                className="termContent"
                style={{
                  height: `${terms}px`,
                }}
              >
                <div className="termsInner" ref={TermsInner}>
                  <h3>Terms &amp; Conditions</h3>
                  <h6 className="bold-black">
                    Expiry: {coupon.date_expiry ? coupon.date_expiry : "Soon"}
                  </h6>
                  <div
                    className="dyncnt"
                    dangerouslySetInnerHTML={{
                      __html: termsLinksNewTab(),
                    }}
                  />
                </div>
              </div>
              <div className="clearfix" />
            </div>
          )}
        </div>
        <div className="clearfix" />
      </div>
    </li>
  )
}

export default HomeCoupon
