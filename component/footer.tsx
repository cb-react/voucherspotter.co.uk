import Link from "next/link"
import { useRouter } from "next/router"
import { env } from "process"
import React, { createRef, RefObject, useState } from "react"
import fetchData from "../clientFetchData"

const Footer = () => {
  let newsletterRow = createRef<HTMLDivElement | any>()
  let [subsVal, setSubsVal] = useState("")
  let [subsValidate, setSubsValidate] = useState("")
  const router = useRouter()
  const link = `${process.env.NEXT_PUBLIC_APP_URL}${
    router.query ? (router.query.page ? router.query.page : "") : ""
  }`
  const subsEntr = (e: KeyboardEventInit) => {
    e.key === "Enter" && subsBtn()
  }
  const subsBtn = () => {
    if (subsVal.length > 0) {
      let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      if (emailRegex.test(subsVal)) {
        fetchData("subscribe", "POST", {
          email: subsVal,
          link,
        })
          .then((data) => {
            if (data.status)
              newsletterRow.current.innerHTML = `<h3>${data.msg}</h3>`
            else {
              let p = document.createElement("p")
              p.style.textAlign = "center"
              p.textContent = data.msg
              newsletterRow.current.querySelector(".large-12").append(p)
              setTimeout(() => p.remove(), 3000)
            }
          })
          .catch((err) => {
            setSubsValidate("error")
            alert("Internal Server Error !")
          })
      } else {
        setSubsValidate("warning")
        alert("Enter The Correct Email Address !")
      }
    } else {
      setSubsValidate("error")
      alert("Enter The Email Address !")
    }
  }
  return (
    <footer role="contentinfo">
      <div className="newsletter-wrap stripe-white">
        <div className="row" ref={newsletterRow}>
          <div className="large-3 columns">
            <h3>SUBSCRIBE NEWSLETTER</h3>
          </div>
          <div className="small-9 large-7 columns">
            <input
              type="text"
              name="emailsubscribe"
              id="footernews"
              placeholder="correo example@domain.com"
              className={`input field primary ${subsValidate}`}
              required
              value={subsVal}
              onChange={(e) => setSubsVal(e.target.value)}
              autoComplete="off"
              onKeyUp={subsEntr}
            />
          </div>
          <div className="small-3 large-2 columns">
            <input
              type="button"
              className="input button primary red"
              defaultValue="Suscríbete"
              onClick={subsBtn}
            />
          </div>
          <div className="clearfix"></div>
          <div className="large-12 columns" style={{ marginTop: 15 }}>
            <p style={{ fontSize: "12px", textAlign: "center" }}>
              By signing up I agree to Voucherspotter.co.uk Terms of Service,
              <Link href="/privacy-policy">
                <a style={{ color: "#00A651" }}> Privacy Policy</a>
              </Link>
              &nbsp; and consent to receive emails about offers.
            </p>
          </div>
        </div>
      </div>
      <div className="stripe-dark pre-footer-wrap">
        <div className="row">
          <div className="large-8 columns">
            <h3 className="alt">ABOUT US</h3>
            <div className="text-content">
              <p>
                Looking for some thrifty exclusive discount deals? Then you have
                reached your final destination. VoucherSpotter is an online
                platform that specializes in offering exhilarating discounts and
                vouchers free of cost from thousands of top international
                brands. With presence in Canada, UK, America and Australia, we
                help create engagement and sales for thousands of brands
                existing internationally.
              </p>
            </div>
          </div>
          <nav className="large-4 columns">
            <h3 className="alt">Menu</h3>
            <ul className="rr footer-menu">
              <li>
                <Link href="/about-us">
                  <a>About Us</a>
                </Link>
              </li>
              <li>
                <Link href="/privacy-policy">
                  <a>Privacy Policy</a>
                </Link>
              </li>
              <li>
                <Link href="/terms-and-conditions">
                  <a>Terms &amp; Conditions</a>
                </Link>
              </li>
              <li>
                <Link href="/contact-details">
                  <a>Contact</a>
                </Link>
              </li>
            </ul>
          </nav>
        </div>
      </div>
      <div className="stripe-dark disclosure">
        <div className="row">
          <div className="large-12">
            <p>
              DISCLOSURE: The links available on our website are associated with
              affiliate networks, when you make a purchase or avail a service by
              clicking on these links it generates a commission for us.
            </p>
          </div>
        </div>
      </div>
      <div className="stripe-darker footer-wrap">
        <div className="row">
          <div className="large-3 columns">
            <div className="copyright">
              Copyright &copy; {new Date().getFullYear()} All rights reserved.
            </div>
          </div>
          <div className="large-3 columns">
            <ul className="rr social">
              <li>
                <Link href="https://facebook.com/">
                  <a
                    className="ir fb"
                    target="_blank"
                    title="Facebook"
                    aria-label="Facebook"
                  />
                </Link>
              </li>
              <li>
                <Link href="https://twitter.com/">
                  <a
                    className="ir tw"
                    target="_blank"
                    title="Twitter"
                    aria-label="Twitter"
                  />
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer
