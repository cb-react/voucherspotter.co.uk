import React from "react";
import Link from "next/link";
import Image from "next/image";

interface ThumbProps {
  thumbs: [];
}

interface Thumb {
  link: string;
  name: string;
  image: string;
  image_mobile: string;
}

const HomeFeaturesThumbs = ({ thumbs }: ThumbProps) => {
  if (thumbs)
    return (
      <div className="main-slider-wrap">
        <div className="row">
          <div className="column">
            <div className="row" style={{ padding: "20px 0" }}>
              {thumbs.map((thumb: Thumb, k) => (
                <div className="large-4 columns" key={k}>
                  <Link href={thumb.link}>
                    <a title={thumb.name}>
                      <Image
                        src={`${process.env.NEXT_PUBLIC_APP_MEDIA}${thumb.image}`}
                        alt={thumb.name}
                        title={thumb.name}
                        width={380}
                        height={300}
                      />
                    </a>
                  </Link>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  return <></>;
};

export default HomeFeaturesThumbs;
