import React, { useState, createRef } from "react"
import { useRouter } from "next/router"
import Image from "next/image"
import StoreData from "../interface/StorePage"
import IStoreCoupon from "../interface/StoreCoupon"

interface StoreCouponProps {
  store: StoreData
  coupon: IStoreCoupon
}

type CouponType = "Code" | "Deal"

const StoreCoupon = ({ store, coupon }: StoreCouponProps) => {
  let router = useRouter()
  let pathname = router.query.page

  let dealCodeBtn = (id: number, type: CouponType) => {
    let urlParam = type === "Code" ? "copy" : "shopnow"
    window.open(
      `${process.env.NEXT_PUBLIC_APP_URL}${pathname}?${urlParam}=${id}`
    )
    location.href = `${process.env.NEXT_PUBLIC_APP_URL}r?id=${id}`
  }

  let Button = ({ id, type }: { id: number; type: CouponType }) => {
    return (
      <a
        className="c_offer input button red secondary responsive"
        onClick={() => dealCodeBtn(id, type)}
      >
        {type === "Code" ? "Show Code" : "Get Deal"}
      </a>
    )
  }

  let [terms, setTerms] = useState(0)

  let TermsInner: { current: any } = createRef()

  let TermsBtn = (e: any) => {
    let termBtnActive: HTMLDivElement | null = document.querySelector(
      ".rr.items-landscape .wrapper-3 .termbtn.active"
    )
    !e.target.classList.contains("active") &&
      termBtnActive &&
      termBtnActive.click()

    return setTerms(
      TermsInner.current.parentElement.offsetHeight === 0
        ? TermsInner.current.offsetHeight + 20
        : 0
    )
  }

  return (
    <li className="wrapper-3">
      <div className="row">
        <div className="large-2 small-12 columns">
          <figure>
            <a>
              <Image
                src={`https://topvoucherscode.co.uk/image/${store.store_image}`}
                title={coupon.title}
                alt={`${store.name} Discount code`}
                width={100}
                height={100}
              />
            </a>
            <span>
              <a />
            </span>
          </figure>
        </div>
        <div className="large-7 small-8 columns">
          <div className="col-middle">
            <h2 className="alt">
              <a
                role="button"
                onClick={() => dealCodeBtn(coupon.id, coupon.code_status)}
                className="c_offer"
              >
                {coupon.title}
              </a>
            </h2>
          </div>
        </div>
        <div className="large-3 small-4 columns">
          <div className="col-right">
            <Button id={coupon.id} type={coupon.code_status} />
            {coupon.coupon_terms && (
              <a
                onClick={TermsBtn}
                className={`termbtn${terms > 0 ? " active" : ""}`}
              >
                Terms <i className="fa fa-info" />
              </a>
            )}
          </div>
        </div>
        <div className="clearfix" />
      </div>
      {coupon.coupon_terms && (
        <div
          className="termContent"
          style={{
            height: `${terms}px`,
          }}
        >
          <div className="termsInner" ref={TermsInner}>
            <h3>Terms &amp; Conditions</h3>
            <h6 className="bold-black">
              Expiry: {coupon.expiry ? coupon.expiry : "Soon"}
            </h6>
            <div
              className="dyncnt"
              dangerouslySetInnerHTML={{ __html: coupon.coupon_terms }}
            />
          </div>
        </div>
      )}
    </li>
  )
}

export default StoreCoupon
