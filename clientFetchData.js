const fetchData = (
  query,
  method = "GET",
  body = undefined,
  headers = {},
  reqInit = {}
) => {
  // Request URL
  const reqInfo = `${process.env.NEXT_PUBLIC_APP_URL}${query}`

  // Request Headers
  const reqHeaders = {
    method,
    ...reqInit,
    headers: {
      "Content-Type": "application/json",
      ...headers,
    },
  }

  // If POST
  if (method === "POST" && body) {
    reqHeaders.body = JSON.stringify(body)
  }

  // Fetch
  return fetch(reqInfo, reqHeaders)
    .then((res) => {
      if (!res.ok) throw new Error(res.statusText)
      else return res.json()
    })
    .catch((err) => console.log(err))
}

module.exports = fetchData
