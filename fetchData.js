const fetchData = (
  query,
  method = "GET",
  body = undefined,
  headers = undefined
) => {
  // Request URL
  const reqInfo = `${process.env.SERVER_URL}${query}`

  // Request Headers
  const reqHeaders = {
    method,
    // mode: "cors",
    // credentials: "include",
    headers: {},
  }

  // If Server Side Fetch Add API Access Token
  if (!process.browser) {
    reqHeaders.headers = {
      hostname: `${process.env.SERVER_HOSTNAME}`,
      "Api-Token": `${process.env.SERVER_TOKEN}`,
    }
  }

  // If Custom Header Included
  if (headers) {
    reqHeaders.headers = {
      ...reqHeaders.headers,
      ...headers,
      "Content-Type": "application/json",
      "Content-Length": "<calculated when request is sent>",
    }
  }

  // If POST
  if (method === "POST" && body) {
    reqHeaders.body = JSON.stringify(body)
  }

  // Fetch
  return fetch(reqInfo, reqHeaders)
    .then((res) => {
      if (res.ok) {
        return res.json()
      }
      return ""
    })
    .catch((err) => {
      throw err
    })
}

module.exports = fetchData
