import dynamic from "next/dynamic"
import Head from "next/head"
import Image from "next/image"
import Link from "next/link"
import { useRouter } from "next/router"
import React, { useState } from "react"
import fetchData from "../clientFetchData"
import StoreCoupon from "../component/StoreCoupon"
import StoreTable from "../component/StoreTable"
import IStoreCoupon from "../interface/StoreCoupon"
import StoreData, {
  StoreRating,
  RelatedCategories,
  TableCoupons,
  Breadcrumd,
  Faqs,
  RelatedStores,
} from "../interface/StorePage"

interface StoreProps {
  title: string
  data: {
    store_data: [StoreData]
    store_rating: StoreRating
    coupons: [IStoreCoupon]
    related_categories: [RelatedCategories]
    table_coupons: [TableCoupons]
    breadcrum: Breadcrumd
    faqs: [Faqs]
    related_stores: [RelatedStores]
  }
}

const Store = ({ data, title }: StoreProps) => {
  const { store_data, store_rating, coupons, table_coupons, breadcrum } = data
  const store = store_data[0]
  const { average, total } = store_rating.original
  let router = useRouter()
  let [averageStar, setAverageStar] = useState(average)
  let [totalStar, setTotalAverageStar] = useState(total)
  const Popup = router.query.copy || router.query.shopnow
  const ModalPopup = Popup
    ? (dynamic(() =>
        import("../component/CouponPopup").then((module: any) => module.default)
      ) as any)
    : () => <></>

  let Description = () => {
    if (store.description) {
      let dscLimit = 97
      let dscLength = store.description.length > dscLimit
      let shortDsc = store.description.slice(0, dscLimit)
      let [toggleDsc, setToggleDsc] = useState(true)
      let [dsc, setDsc] = useState({ __html: shortDsc })

      return (
        <>
          <div className="text-content">
            <div dangerouslySetInnerHTML={dsc} />
            {dscLength && (
              <p>
                <a
                  className="morelink"
                  onClick={() => (
                    toggleDsc
                      ? setDsc({ __html: store.description })
                      : setDsc({ __html: shortDsc }),
                    setToggleDsc(!toggleDsc)
                  )}
                >
                  Show {toggleDsc ? "More" : "Less"}
                </a>
              </p>
            )}
          </div>
        </>
      )
    }
    return <></>
  }

  let StoreRating = () => {
    let star = 0
    let stars = []

    let storeRatingHandler = (star: number) => {
      fetchData("storeRating", "POST", { star, id: store.id }).then((data) => {
        data.status
          ? (setAverageStar(data.averageRating),
            setTotalAverageStar(data.totalRating))
          : ((document.querySelector(".ratingCalculator") as any).textContent =
              "You have already rated !")
      })
    }

    while ((star as Number) < 5) {
      star++
      stars.push(
        <span
          key={star}
          onClick={() => storeRatingHandler(star + 1)}
          className={`ico ${star <= average && "RateActive"}`}
        />
      )
    }
    return <>{stars}</>
  }

  const pageTitle = title.length > 0 ? title : store.page_title

  return (
    <>
      <div className="stripe-regular">
        <Head>
          <meta name="description" content={store.meta_description} />
          <meta name="keywords" content={store.meta_keywords} />
          <title>{pageTitle}</title>
          <script
            type="application/ld+json"
            dangerouslySetInnerHTML={{
              __html: `[
            {
              "@context": "http://schema.org",
              "@type": "Store",
              "@id": "${process.env.NEXT_PUBLIC_APP_URL}", 
              "url": "${router.asPath}",
              "image": "${store.store_image}",
              "name": "${store.name}",
              "aggregateRating": {
                "@type": "AggregateRating",
                "bestRating": "5",
                "worstRating": "1",
                "ratingValue": "${average}",
                "reviewCount": "${total}"
              },
              "address": {
                "@type": "PostalAddress",
                "addressLocality": "Charleston, SC 29424",
                "addressRegion": "United States",
                "streetAddress": "820 Broadway Street",
                "telephone": "+1 843-789-6897"
              }
            }
          ]`,
            }}
          />
        </Head>
        <div className="row">
          <div className="large-9 columns">
            <div className="coupons-wrapper">
              <div className="row">
                <div className="large-8 columns">
                  <h2>{title ? title : `${store.name} Promo Codes & Deals`}</h2>
                </div>
              </div>
              <ul className="rr items-landscape">
                {coupons ? (
                  <>
                    {coupons.map((coupon, key) => (
                      <StoreCoupon coupon={coupon} store={store} key={key} />
                    ))}
                  </>
                ) : (
                  <li>No coupons !</li>
                )}
              </ul>
            </div>
            <StoreTable title={title} coupons={table_coupons} />
          </div>
          <aside className="large-3 columns main-sidebar">
            <div className="row collapse store-info">
              <figure>
                <Image
                  width={118}
                  height={118}
                  src={`${process.env.NEXT_PUBLIC_APP_MEDIA}${store.store_image}`}
                  alt={store.name}
                />
              </figure>
              <div className="space-left">
                <h2 className="alt">
                  {store.title_name ??
                    `${store.name} Promo Codes & Deals Storename`}
                </h2>
                {store.description && <Description />}
              </div>
            </div>

            <div className="row collapse rating-widget">
              <div className="rating">
                <h3>Rating</h3>
                <StoreRating />
                <p className="ratingCalculator">
                  Rated {averageStar} from {totalStar} votes
                </p>
              </div>
            </div>

            <ul
              className="brdcrb"
              itemScope
              itemType="https://schema.org/BreadcrumbList"
            >
              <li
                itemProp="itemListElement"
                itemScope
                itemType="https://schema.org/ListItem"
              >
                <a itemProp="item" href={`${router.basePath}/`}>
                  <span itemProp="name">Home</span>
                  <meta itemProp="position" content="1" />
                </a>
              </li>
              <li
                itemProp="itemListElement"
                itemScope
                itemType="https://schema.org/ListItem"
              >
                <a itemProp="item" href={`${router.basePath}/categories`}>
                  <span itemProp="name">Categories</span>
                  <meta itemProp="position" content="2" />
                </a>
              </li>
              {breadcrum && (
                <li
                  itemProp="itemListElement"
                  itemScope
                  itemType="https://schema.org/ListItem"
                >
                  <a
                    itemProp="item"
                    href={`${router.basePath}/${breadcrum.slug}`}
                  >
                    <span itemProp="name">{breadcrum.name}</span>
                    <meta itemProp="position" content="3" />
                  </a>
                </li>
              )}
              <li
                itemProp="itemListElement"
                itemScope
                itemType="https://schema.org/ListItem"
              >
                <a itemProp="item" href={`${router.asPath}`}>
                  <span itemProp="name">{store.name}</span>
                  <meta itemProp="position" content={breadcrum ? "4" : "3"} />
                </a>
              </li>
            </ul>
          </aside>
        </div>
      </div>
      <ModalPopup defaultTitle={pageTitle} id={Popup} />
    </>
  )
}

export default Store
