import Link from "next/link"
import React from "react"
import IStoresThumbnial from "../interface/StoresThumbnial"

interface StoresThumbnial {
  data: any
  title: string
}

const StoresThumbnial = ({ data, title }: StoresThumbnial) => {
  return (
    <div className="stripe-regular">
      <div className="row">
        <div className="large-12 columns">
          <h2 style={{ color: "#000000 !important" }}> {title} </h2>
          <div className="categories row">
            {data &&
              data.data.top_categories.map((store: any, key: number) => (
                <div className="large-2 columns" key={key}>
                  <div className="cat-icon hvr-radial-out">
                    <Link href={`/${store.url}`}>
                      <a title={store.store_name}>
                        <img
                          src={store.image}
                          alt={store.store_name}
                          title={store.store_name}
                        />
                      </a>
                    </Link>
                  </div>
                  <div className="cat-text">
                    <Link href={`/${store.url}`}>
                      <a>{store.store_name}</a>
                    </Link>
                  </div>
                </div>
              ))}
            <div className="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default StoresThumbnial
