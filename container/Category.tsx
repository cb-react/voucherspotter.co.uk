import Link from "next/link"
import React from "react"
import IStoresThumbnial from "../interface/StoresThumbnial"
import CategoryDetails from "../interface/CategoryDetails"
import Head from "next/head"
import Coupon from "../interface/Coupon"

interface StoresThumbnial {
  data: {
    category: [CategoryDetails]
    stores: [IStoresThumbnial]
    coupons: [Coupon]
  }
  title: string
}

const StoresThumbnial = ({ data, title }: StoresThumbnial) => {
  const { category, stores } = data
  let cdtl = category[0]
  return (
    <div className="stripe-regular">
      <Head>
        <title>{cdtl.meta_title}</title>
        <meta name="description" content={cdtl.meta_description} />
        <meta name="keywords" content={cdtl.meta_keyword} />
      </Head>
      <div className="row">
        <div className="large-12 columns">
          <h2 style={{ color: "#000000 !important" }}> {title} </h2>
          <div className="categories row">
            {stores.map((store, key) => (
              <div className="large-2 columns" key={key}>
                <div className="cat-icon hvr-radial-out">
                  <Link href={`/${store.slug}`}>
                    <a title={store.store_name}>
                      <img
                        src={store.image}
                        alt={store.store_name}
                        title={store.store_name}
                      />
                    </a>
                  </Link>
                </div>
                <div className="cat-text">
                  <Link href={`/${store.slug}`}>
                    <a>{store.store_name}</a>
                  </Link>
                </div>
              </div>
            ))}
            <div className="clearfix" />
          </div>
        </div>
      </div>
    </div>
  )
}

export default StoresThumbnial
