import Link from "next/link";
import React from "react";
import Category from "../interface/Category";

interface CategoriesProps {
  data: [Category];
  title: string;
}

const CategoriesThumbnial = ({ data, title }: CategoriesProps) => {
  return (
    <div className="stripe-regular">
      <div className="row">
        <div className="large-12 columns">
          <h2
            style={{ color: "#000000 !important" }}
            dangerouslySetInnerHTML={{ __html: title }}
          />
          <div className="categories row">
            {data.map((category, key) => (
              <div className="large-2 columns" key={key}>
                <div className="cat-icon hvr-radial-out">
                  <Link href={`/${category.page_url}`}>
                    <a title={category.name}>
                      <img
                        src={`${process.env.NEXT_PUBLIC_APP_MEDIA}${category.image}`}
                        alt={category.name}
                        title={category.name}
                      />
                    </a>
                  </Link>
                </div>
                <div className="cat-text">
                  <Link href={`/${category.page_url}`}>
                    <a dangerouslySetInnerHTML={{ __html: category.name }} />
                  </Link>
                </div>
              </div>
            ))}
            <div className="clearfix" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default CategoriesThumbnial;
