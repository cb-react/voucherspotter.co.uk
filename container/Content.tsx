import { NextPage } from "next"
import Head from "next/head"
import React from "react"
import IContent from "../interface/Information"

const Content: NextPage<IContent> = ({ data, title }) => {
  if (data) {
    return (
      <div className="stripe-regular">
        <Head>
          <meta name="description" content={data.meta_description} />
          <meta name="keywords" content={data.meta_keywords} />
        </Head>
        <div className="row">
          <div className="large-12 columns">
            <article className="blog-post-content">
              <section className="article-main-content">
                <div className="wrapper-6 primary">
                  <div className="row">
                    <div className="column">
                      <h2 className="alt">{data.information.title}</h2>
                      <div
                        className="text-content"
                        dangerouslySetInnerHTML={{
                          __html: data.information.description
                            ? data.information.description
                            : "",
                        }}
                      />
                    </div>
                  </div>
                </div>
              </section>
            </article>
          </div>
        </div>
      </div>
    )
  } else {
    return <></>
  }
}

export default Content
