import Head from "next/head"
import HomeFeaturesThumbs from "../component/homeFeaturesThumbs"
import { useState } from "react"
import HomeCoupon from "../component/homeCoupon"
import Link from "next/link"
import dynamic from "next/dynamic"
import fetchData from "../fetchData"
import Image from "next/image"
import { GetServerSideProps, GetServerSidePropsContext, NextPage } from "next"
import ICoupon from "../interface/Coupon"
import { useRouter } from "next/router"

interface ICategory {
  id: number
  name: string
  slug: string
  image: string
}

interface IHomeProps {
  home_banners?: []
  featured_coupons?: [ICoupon]
  top_categories?: [ICategory]
  notFound?: object
}

const Home: NextPage<IHomeProps & object> = ({
  home_banners,
  featured_coupons,
  top_categories,
  notFound,
}) => {
  const router = useRouter()
  const Popup = router.query.copy || router.query.shopnow
  const ModalPopup = Popup
    ? (dynamic(() =>
        import("../component/CouponPopup").then((module: any) => module.default)
      ) as any)
    : () => ""
  return (
    <div>
      <Head>
        <title>VoucherSpotter.co.uk | Place to Save</title>
      </Head>
      {!notFound ? (
        <>
          {home_banners && <HomeFeaturesThumbs thumbs={home_banners} />}
          <div role="main">
            <div className="stripe-white big items-carousel-wrap">
              <div className="row">
                <div className="large-9 column">
                  {featured_coupons && (
                    <div>
                      <h2>Recommended Coupons</h2>
                      <ul className="rr slides">
                        {featured_coupons.map((coupon, k) => (
                          <HomeCoupon coupon={coupon} key={k} />
                        ))}
                      </ul>
                    </div>
                  )}
                </div>
                <div className="large-3 column">
                  {top_categories && (
                    <div className="topStores">
                      <h2>Top Stores</h2>
                      <div className="logWrpr">
                        {top_categories.map((category, k) => {
                          return (
                            <div className="large-6 columns" key={k}>
                              <Link href={`/${category.slug}`}>
                                <a>
                                  <span className="topLogo">
                                    <Image
                                      src={`${process.env.NEXT_PUBLIC_APP_MEDIA}${category.image}`}
                                      alt={category.name}
                                      title={category.name}
                                      width={108}
                                      height={108}
                                    />
                                  </span>
                                </a>
                              </Link>
                            </div>
                          )
                        })}
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
          <ModalPopup
            defaultTitle={process.env.NEXT_PUBLIC_SITETITLE}
            id={Popup}
          />
        </>
      ) : (
        <></>
      )}
    </div>
  )
}

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext
) => {
  let props = {}

  // Retrieve Home Page Data
  props = await fetchData("home")

  return { props }
}

export default Home
