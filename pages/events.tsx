import Link from "next/link";
import React from "react";
import EventThumbnail from "../interface/EventThumbnail";
import Head from "next/head";
import { GetStaticProps, GetStaticPropsContext, NextPage } from "next";
import fetchData from "../fetchData";

interface EventsProp {
  events: [EventThumbnail] | null;
}

const Events: NextPage<EventsProp> = ({ events }) => {
  return (
    <div className="stripe-regular">
      <Head>
        <title>Events {process.env.NEXT_PUBLIC_INNERPAGE_TITLE}</title>
      </Head>
      <div className="row">
        <div className="large-12 columns">
          <h2 style={{ color: "#000000 !important" }}>Events</h2>
          {events && (
            <div className="categories row">
              {events.map((event, key) => (
                <div className="large-2 columns" key={key}>
                  <div className="cat-icon hvr-radial-out">
                    <Link href={`/${event.slug}`}>
                      <a title={event.name}>
                        <img
                          src={event.image}
                          alt={event.name}
                          title={event.name}
                        />
                      </a>
                    </Link>
                  </div>
                  <div className="cat-text">
                    <Link href={`/${event.slug}`}>
                      <a>{event.name}</a>
                    </Link>
                  </div>
                </div>
              ))}
              <div className="clearfix" />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export const getStaticProps: GetStaticProps = async (
  ctx: GetStaticPropsContext
) => {
  const props: EventsProp = { events: null };

  await fetchData("events").then((data) => {
    props.events = data.events;
  });

  return { props };
};

export default Events;
