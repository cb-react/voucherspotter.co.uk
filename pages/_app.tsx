import Head from "next/head"
import type { AppProps } from "next/app"

import "../styles/normalize.css"
import "../styles/foundation.css"
import "../styles/flexSlider.css"
import "../styles/style.css"
import "../styles/main.css"
import "../styles/mq-32.css"
import "../styles/mq-48.css"
import "../styles/mq-67.css"
import "../styles/font-awesome.css"
import Header from "../component/header"
import Footer from "../component/footer"
import { useRouter } from "next/router"
import { useEffect } from "react"

const MyApp = (props: AppProps) => {
  const { Component, pageProps } = props
  const router = useRouter()
  const routerQuery =
    (typeof router.query.page === "object"
      ? router.query.page[0]
      : router.query.page) ?? router.pathname
  const uppercaseDetect = /[A-Z]/.test(routerQuery)
  useEffect(() => {
    uppercaseDetect &&
      router.push(routerQuery.toLowerCase(), undefined, {
        shallow: true,
      })
  }, [])
  return (
    <>
      <Header />
      <Head>
        <link rel="preload" as="style" href="./fonts.css" />
        <link rel="stylesheet" href="./fonts.css" />

        <meta
          name="description"
          content="VoucherSpotter provides you with the most effective and consistent discounts, in the form of vouchers, coupons, offers, and promotions, get them all. December 2020"
        />
        <meta
          name="keywords"
          content="coupon codes, promo codes, discount codes, voucher codes, coupon deals, top coupons, best coupons, coupons for stores, coupon discounts"
        />
        <meta
          property="al:web:url"
          content="https://www.voucherspotter.co.uk/"
        />
        <meta name="verify-admitad" content="17eb7de57c" />
        <meta
          name="google-site-verification"
          content="J5RG3_MQJM7ZEEMg_5upwlV0AnHH4jhrwJxbf-kj2HE"
        />
        <link rel="icon" href="./favicon.png" />

        {/* Organisation Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: `{ "@context": "http://schema.org", "@type": "Organization", "name": "VoucherSpotter", "url": "https://www.topvoucherscode.co.uk/", "sameAs": [ "https://www.facebook.com/topvoucherscode", "https://www.instagram.com/topvoucherscode.co.uk/", "https://twitter.com/Topvoucherscode", "https://www.youtube.com/watch?v=cxjT21T5yeQ" ] }`,
          }}
        />

        {/* Website Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: `{ "@context": "https://schema.org", "@type": "WebSite", "url": "https://www.topvoucherscode.co.uk/", "potentialAction": { "@type": "SearchAction", "target": "https://www.topvoucherscode.co.uk/storesearch?q={search_term_string}", "query-input": "required name=search_term_string" } }`,
          }}
        />

        {/* Google Tag Manager */}
        <script
          dangerouslySetInnerHTML={{
            __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-PP8VCFT');`,
          }}
        />
      </Head>
      <Component {...pageProps} />
      <Footer />
    </>
  )
}

export default MyApp
