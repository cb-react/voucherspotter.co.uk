import { NextPage } from "next";
import Head from "next/head";
import React from "react";

const Contact: NextPage = () => {
  return (
    <>
      <Head>
        <title>Contact Details {process.env.NEXT_PUBLIC_INNERPAGE_TITLE}</title>
      </Head>
      <div className="stripe-regular">
        <div className="row">
          <div className="large-12 columns">
            <article className="blog-post-content">
              <section className="article-main-content">
                <div className="wrapper-6 primary">
                  <div className="row">
                    <div className="column">
                      <h2 className="alt">Contact Us</h2>
                      <div className="text-content">
                        <p style={{ fontSize: "18px" }}>
                          Address: AB55 4AB, Balvenie St Dufftown, Keith, <br />
                          United Kingdom
                          <br />
                          Telephone: +447913578691
                          <br />
                          Email: affiliates@voucherspotter.co.uk
                        </p>
                        <iframe
                          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2146.73681558921!2d-3.13225528400295!3d57.44875098104085!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4885a567aef65fab%3A0xe89faa5cc183acc4!2sBalvenie+St%2C+Dufftown%2C+Keith+AB55+4AS%2C+UK!5e0!3m2!1sen!2s!4v1506950512206"
                          width={800}
                          height={300}
                          frameBorder={0}
                          style={{ border: 0 }}
                          allowFullScreen
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </article>
          </div>
        </div>
      </div>
    </>
  );
};

export default Contact;
