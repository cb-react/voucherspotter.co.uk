import Head from "next/head";
import React from "react";
import CategoriesThumbnial from "../container/CategoriesThumbnial";
import fetchData from "../fetchData";
import { GetStaticProps, GetStaticPropsContext, NextPage } from "next";
import Category from "../interface/Category";

interface CategoriesProps {
  categories: [Category];
}

const Categories: NextPage<CategoriesProps> = ({ categories }) => {
  return (
    <>
      <Head>
        <title>Categories {process.env.NEXT_PUBLIC_INNERPAGE_TITLE}</title>
      </Head>
      {categories && (
        <CategoriesThumbnial title="Categories" data={categories} />
      )}
    </>
  );
};

export const getStaticProps: GetStaticProps = async (
  ctx: GetStaticPropsContext
) => {
  let props = {};

  // Retrieve Home Page Data
  props = await fetchData("categories");

  return { props };
};

export default Categories;
