// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import fetchData from "../../fetchData";

export default (req, res) => {
  fetchData("home")
    .then((data) => {
      res.statusCode = 200;
      res.setHeader("Content-Type", "application/json");
      res.end(JSON.stringify(data));
    })
    .catch((err) => {
      console.log("error");
      console.log(err);
      res.statusCode = 404;
    });
};
