import { GetServerSideProps, GetServerSidePropsContext, NextPage } from "next"
import Head from "next/head"
import { useRouter } from "next/router"
import React from "react"
import StoresThumbnial from "../container/StoresThumbnial"
import IStoresThumbnial from "../interface/StoresThumbnial"
import fetchData from "../fetchData"

interface ResultsProps {
  results: [IStoresThumbnial]
}

const StoreSearch: NextPage<ResultsProps> = ({ results }) => {
  const router = useRouter()
  const query = router.query.q ? router.query.q : ""
  return (
    <>
      <Head>
        <title>Search - {query}</title>
      </Head>
      <StoresThumbnial title={`Search - ${query}`} data={results} />
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext
) => {
  let props: { results: null | [] } = { results: null }

  console.log(ctx.query.q)

  // Retrieve Store Search Default Data
  await fetchData("get_default_search").then((results) => {
    props = { results }
  })

  return { props }
}

export default StoreSearch
