import React, { useState } from "react"
import Store from "../container/Store"
import Head from "next/head"
import Content from "../container/Content"
import fetchData from "../fetchData"
import { GetServerSideProps, GetServerSidePropsContext, NextPage } from "next"
import Category from "../container/Category"
import Error404 from "./404"
import PageProps from "../interface/Page"

const Page: NextPage<PageProps> = ({ page_title, data, page_type }) => {
  const [defaultTitle, changeTitle] = useState(page_title ?? "")
  const props = { title: defaultTitle, data }

  const PageTemplate = () => {
    switch (page_type) {
      case "store":
        return <Store {...props} />
        break
      case "information":
        return <Content {...props} />
        break
      case "category":
        return <Category {...props} />
        break
      case "event":
        return <Category {...props} />
        break
      default:
        return <Error404 />
        break
    }
  }
  return (
    <>
      <Head>
        <title>
          {defaultTitle} {process.env.NEXT_PUBLIC_INNERPAGE_TITLE}
        </title>
      </Head>
      <PageTemplate />
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext
) => {
  let props: PageProps = {}

  // Fetch If Slug or Page Path Exist To Server API or Return Not Found Object
  props = await fetchData(ctx.query.page)

  // Return Above Result
  return { props }
}

export default Page
