const express = require("express")
const bodyParser = require("body-parser")
const fetchData = require("../fetchData")

const subscribe = express.Router()
subscribe.use(bodyParser.json())
subscribe
  // .post(parseForm, csrfProtection, (req, res, next) => {
  .post("/", (req, res, next) => {
    const { email, link } = req.body
    fetchData("newslettersignup", "POST", {
      email,
      link,
    })
      .then((resp) => {
        res.statusCode = 200
        res.setHeader("Content-Type", "application/json")
        res.json(resp.data)
      })
      .catch((err) => {
        res.statusCode = 404
        next(err)
      })
  })

module.exports = subscribe
