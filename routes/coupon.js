const express = require("express")
const bodyParser = require("body-parser")
const fetchData = require("../fetchData")

const coupon = express.Router()
coupon.use(bodyParser.json())
coupon
  // .post(parseForm, csrfProtection, (req, res, next) => {
  .post("/", (req, res, next) => {
    const { id } = req.body

    fetchData(`aff_redirect?c_id=${id}`, "GET")
      .then((resp) => {
        res.statusCode = 200
        res.setHeader("Content-Type", "application/json")
        res.json(resp)
      })
      .catch((err) => {
        res.statusCode = 404
        next(err)
      })
  })

module.exports = coupon
