const express = require("express")
const bodyParser = require("body-parser")
const fetchData = require("../fetchData")

const coupon = express.Router()
coupon.use(bodyParser.json())
coupon
  // .post(parseForm, csrfProtection, (req, res, next) => {
  .get("/:id", (req, res, next) => {
    const id = parseInt(req.params.id)

    if (id) {
      fetchData(`aff_redirect?c_id=${id}`, "GET")
        .then((resp) => {
          res.redirect(resp.aff_link)
        })
        .catch((err) => {
          res.redirect(process.env.NEXT_PUBLIC_APP_URL)
        })
    } else {
      res.redirect(process.env.NEXT_PUBLIC_APP_URL)
    }
  })

module.exports = coupon
