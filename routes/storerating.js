const express = require("express")
const bodyParser = require("body-parser")
const fetchData = require("../fetchData")

const storerating = express.Router()
storerating.use(bodyParser.json())
storerating
  // .post(parseForm, csrfProtection, (req, res, next) => {
  .post("/", (req, res, next) => {
    const ip = req.connection.remoteAddress
    const { store, value } = req.body
    fetchData("storeRating", "POST", { store, value, ip })
      .then((resp) => {
        res.statusCode = 200
        res.setHeader("Content-Type", "application/json")
        res.json(resp)
      })
      .catch((err) => {
        res.statusCode = 404
        next(err)
      })
  })

module.exports = storerating
