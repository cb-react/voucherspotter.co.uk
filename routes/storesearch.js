const express = require("express")
const bodyParser = require("body-parser")
const fetchData = require("../fetchData")

const storesearch = express.Router()

storesearch.use(bodyParser.json())

storesearch.post("/search", (req, res, next) => {
  fetchData("getSearch", "GET", undefined, {
    search: req.body.search_keyword,
  })
    .then((resp) => {
      res.statusCode = 200
      res.setHeader("Content-Type", "application/json")
      res.json(resp.data)
    })
    .catch((err) => {
      res.statusCode = 404
      next(err)
    })
})

module.exports = storesearch
